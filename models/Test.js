var keystone = require('keystone');
var Types = keystone.Field.Types;


/**
 * Test Products Model
 * =============
 */

var Test = new keystone.List('test',{
	map: {name: 'title', slug: 'title'},
	autokey: { from: 'title', path: 'title', unique: true }
});


Test.add({
	title: { type: String, required: true, initial: "Products" },
	imageSide: {type: Types.Select, options: ['Left Side', 'Right Side'], default: 'Left Side'},
	textSide: {type: Types.Select, options: ['Left Side', 'Right Side'], default: 'Right Side'},
	image: { type: Types.CloudinaryImage},
	buttonText: {type: String},
	introText: {type: String},
	features: {type: Types.Html, wysiwyg: true, height: 300}
});

Test.register();

