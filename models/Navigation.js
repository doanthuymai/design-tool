var keystone = require('keystone');
var Types = keystone.Field.Types;


/**
 * Navigation Model
 * =============
 */

var Navigation = new keystone.List('navigation',{
	map: {name: 'title', slug: 'title'},
	autokey: { from: 'title', path: 'title', unique: true }
});


Navigation.add({
	logo: {type: Types.CloudinaryImage},
	title: { type: String, required: true, initial: "Navigation" },
	buttonText: {type: String},
	buttonSlug: {type: Types.Url},
	buttonColor: {type: Types.Color}
});

Navigation.register();

