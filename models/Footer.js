var keystone = require('keystone');
var Types = keystone.Field.Types;

/**
 * Footer Model
 * =============
 */

var Footer = new keystone.List('Footer',{
	map: {name: 'title', slug: 'title'},
	autokey: { from: 'title', path: 'title', unique: true }
});


Footer.add({
	backgroundImage: {type: Types.CloudinaryImage},
	backgroundColor: {type: Types.Color},
	title: { type: String, required: true, initial: "Footer" },
	content:{
		mainContent: {	type: Types.Html, wysiwyg: true, height: 250 	}
	},
	textColor: {type: Types.Color},
	buttonText: {type: String},
	buttonSlug: {type: Types.Url},
	buttonColor: {type: Types.Color}
});

Footer.register();

