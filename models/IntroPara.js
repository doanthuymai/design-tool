var keystone = require('keystone');


/**
 * Intro paragraph Model
 * =============
 */

var IntroPara = new keystone.List('intro-para',{
	map: {name: 'title', slug: 'title'},
	autokey: { from: 'title', path: 'title', unique: true }
});


IntroPara.add({
	title: { type: String, required: true, initial: "Navigation" },
	bigText: {type: String},
	smallText: {type: String}
});

IntroPara.register();

