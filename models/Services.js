var keystone = require('keystone');
var Types = keystone.Field.Types;

/**
 * Navigation Model
 * =============
 */

var Services = new keystone.List('services',{
	map: {name: 'title', slug: 'title'}

});


Services.add({
	
		title: 								{type: String},
	
	
		smallDescription: 					{type: Types.Text},
	
	
	
		firstServiceName: 					{type: String},
		firstServiceDescription: 			{type: String},
		firstServiceImage: 					{type: Types.CloudinaryImage},
	
		secondServiceName: 					{type: String},
		secondServiceDescription: 			{type: String},
		secondServiceImage: 				{type: Types.CloudinaryImage},
	
	
		thirdServiceName: 					{type: String},
		thirdServiceDescription:			{type: String},
		thirdServiceImage: 					{type: Types.CloudinaryImage}
	
});

Services.register();

