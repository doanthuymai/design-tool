var keystone = require('keystone');
var Types = keystone.Field.Types;


/**
 * Slider Model
 * ==========
 */

var Slider = new keystone.List('slider', {
	map: { name: 'title' }
});

Slider.add({
	title: { type: String, required: true },
	backgroundImage: { type: Types.CloudinaryImage },
	bigText: {type: String},
	smallText: {type: String},
	buttonText: {type: String},
	buttonSlug: {type: Types.Url}
});

Slider.register();
