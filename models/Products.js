var keystone = require('keystone');
var Types = keystone.Field.Types;
var str1 = "Image on left side";
var str2 = "Image on the right";

/**
 * Products Model
 * =============
 */

var Products = new keystone.List('products',{
	map: {name: 'title', slug: 'title'}
});


Products.add({
	title: { type: String, required: true, initial: "Products" },
	componentSide: {type: Types.Select, options: [str1, str2], default: str1},
	image: { type: Types.CloudinaryImage},
	buttonText: {type: String},
	buttonSlug: {type: Types.Url},
	introText: {type: String},
	content: {
		description: {type: Types.Html, wysiwyg: true, height: 400}
	},
	backgroundColor: { type: Types.Color }
});

Products.register();

