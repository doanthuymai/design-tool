// Simulate config options from your production environment by
// customising the .env file in your project's root folder.
require('dotenv').load();

// Require keystone
var keystone = require('keystone');
var swig = require('swig');

// Disable swig's bulit-in template caching, express handles it
swig.setDefaults({ cache: process.env.NODE_ENV === 'development' ? false : 'memory' });

keystone.init({

	'name': 'My Site',
	'brand': 'My Site',
	'sass': 'public',
	'static': 'public',
	'favicon': 'public/favicon.ico',
	'views': 'templates/views',
	'view engine': 'swig',
	'custom engine': swig.renderFile,
	'emails': 'templates/emails',
	'auto update': true,
	'session': true,
	'auth': true,
	'user model': 'User'

});

keystone.import('models');

keystone.set('locals', {
	_: require('underscore'),
	env: keystone.get('env'),
	utils: keystone.utils,
	editable: keystone.content.editable
});

keystone.set('routes', require('./routes'));


keystone.set('email locals', {
	logo_src: '/images/logo-email.gif',
	logo_width: 194,
	logo_height: 76,
	theme: {
		email_bg: '#f9f9f9',
		link_color: '#2697de',
		buttons: {
			color: '#fff',
			background_color: '#2697de',
			border_color: '#1a7cb7'
		}
	}
});

keystone.set('email tests', require('./routes/emails'));



keystone.set('nav', {
	header: ['navigation', 'slider', 'intro-para'],
	// posts: ['posts', 'post-categories'],
	// othersPartials: ['photos', 'enquiries'],
	mainBody: ['products','services'],
	footer: 'Footer',
	users: 'users',
	test: 'test'
});

keystone.start();
