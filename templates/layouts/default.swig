{% import "../mixins/flash-messages.swig" as FlashMessages %}

<!doctype html>
<html>
<head>
	<meta charset="utf-8">
	<meta name="viewport" content="width=device-width, initial-scale=1.0">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<title>Design Tool</title>
	<link rel="shortcut icon" href="/favicon.ico" type="image/x-icon">

	<!-- Latest compiled and minified CSS -->
	<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap.min.css" integrity="sha384-1q8mTJOASx8j1Au+a5WDVnPi2lkFfwwEAa8hDDdjZlpLegxhjVME1fgjWPGmkzs7" crossorigin="anonymous">
	<link rel="stylesheet" href="/styles/owl.carousel.css">
	{#<link rel="stylesheet" href="/styles/site.scss">#}
	<link rel="stylesheet" href="/styles/style.css">
	
	{#font#}
	<link href='https://fonts.googleapis.com/css?family=Roboto+Condensed:700&subset=vietnamese' rel='stylesheet'
		  type='text/css'>
	<link href='https://fonts.googleapis.com/css?family=Roboto:100&subset=vietnamese' rel='stylesheet' type='text/css'>
	<link href='https://fonts.googleapis.com/css?family=Roboto:300&subset=vietnamese' rel='stylesheet' type='text/css'>
	
	
	{#if user can access => can see#}
	{%- if user and user.canAccessKeystone -%}
		<link href="/keystone/styles/content/editor.min.css" rel="stylesheet">
	{%- endif -%}
	
	
	<!--[if lt IE 9]>
		<script src="//cdn.jsdelivr.net/html5shiv/3.7.3/html5shiv.js"></script>
		<script src="//cdn.jsdelivr.net/respond/1.4.2/respond.min.js"></script>
	<![endif]-->
</head>


<body class="container-fluid">
	{# HEADER #}
		
			{# Customise your site"s navigation by changing the navLinks Array in ./routes/middleware.js
			   ... or completely change this header to suit your design. #}
		
	{#Just include Navigation in Index.swig#}
	
	{#END HEADER#}
	
	
	{# BODY #}
		<div id="body">
			{# NOTE:
			   There is no .container wrapping class around body blocks to allow more flexibility in design.
			   Remember to include it in your templates when you override the intro and content blocks! #}

			{# The Intro block appears above flash messages (used for temporary information display) #}
			{#THE INTRO#}

			
			{# Flash messages allow you to display once-off status messages to users, e.g. form
			   validation errors, success messages, etc. #}
			{{ FlashMessages.renderMessages(messages) }}

			{# The content block should contain the body of your template"s content #}
			{#THE MAIN CONTENT#}
			{%- block content -%}
			{%- endblock -%}
			
		</div>
	{#END BODY#}
	
	
	{# FOOTER #}
		{#Just include footer partial in index.swig#}
	{#END FOOTER#}
	
	
	
	
		{# JAVASCRIPT #}
		<script src="/js/jquery/jquery-1.11.3.min.js"></script>
		<script src="/js/bootstrap/bootstrap-3.3.5.min.js"></script>
		<!--Libs-->
		{#<script src="libs/jquery-2.2.4.min.js"></script>#}
		{#<script src="libs/bootstrap.js"></script>#}
	
	
		<script src="js/libs/owl.carousel.js"></script>
		<script src="js/libs/main.js"></script>
	
		<script>
			$('.carousel').carousel({
				interval: 0
			})
	
		</script>
		{# The KeystoneJS Content Editor provides support for ks-editable data attributes, which generate links to edit content for users who can access Keystone #}
		{% if user and user.canAccessKeystone -%}
			<script src="/keystone/js/content/editor.js"></script>
		{%- endif -%}
	

	
	</body>
</html>
