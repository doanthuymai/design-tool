
var keystone = require('keystone');
var async = require('async');

exports = module.exports = function(req, res) {
	var view = new keystone.View(req, res);
	var locals = res.locals;
	locals.data = {
		productsListTest: []
		
	};
	
	locals.section = "home";
	view.on('init', function(next){

		var product = keystone.list('products').model.find().sort('name');

		product.exec(function(err, results){
			locals.data.productsList = results;
			next(err);
		});
	});


	view.render('test');
};
