var keystone = require('keystone');

exports = module.exports = function (req, res) {

	var view = new keystone.View(req, res);
	var locals = res.locals;

	// locals.section is used to set the currently selected
	// item in the header navigation.

	
	// All data in the index is retrieved on here
	locals.data = {
		navigation: [],
		slider: [],
		introPara: [],
		productList: [],
		footer: [],
		service: []
	};
	
	
	view.on('init', function (next) {
		
		var navigation = keystone.list('navigation').model.findOne();
		
		navigation.exec(function(err, results){
			locals.data.navigation = results;
			next(err);
		});
	});
	
	view.on('init', function(next){
		
		var slider = keystone.list('slider').model.findOne();
		
		slider.exec(function(err, results){
			locals.data.slider = results;
			next(err);
		});
	});

	view.on('init', function (next) {
		
		var introPara = keystone.list('intro-para').model.findOne();

		introPara.exec(function(err, results){
			locals.data.introPara = results;
			next(err);
		});
	});
	
	
	view.on('init', function(next){
		
		var product = keystone.list('products').model.find().sort('name');
		
		product.exec(function(err, results){
			locals.data.productsList = results;
			next(err);
		});
	});

	view.on('init', function(next){

		var service = keystone.list('services').model.findOne();

		service.exec(function(err, results){
			locals.data.service = results;
			next(err);
		});
	});
	
	view.on('init', function(next){

		var footer = keystone.list('Footer').model.findOne();

		footer.exec(function(err, results){
			locals.data.footer = results;
			next(err);
		});
	});
	
	
	
	// Render the view
	view.render('index');
};

