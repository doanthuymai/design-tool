(function($) {
    "use strict";
    $(document).ready(function() {
        $.ajax( { 
          url: '//freegeoip.net/json/', 
          type: 'POST', 
          dataType: 'jsonp',
          success: function(location) {
              // console.log(location);
              // if(location.country_code === 'VN'){
              //     if( window.location.href.search("vi") !== 23){
              //         window.location.href = '/';
              //     }
              // }
          }
        });
        var testi1 = $(".testimonials-slider .testimonial-content");
        var testi2 = $(".testimonials-slider .testimonial-images");

        testi1.owlCarousel({
            singleItem: true,
            slideSpeed: 1000,
            navigation: false,
            pagination: false,
            afterAction: syncPosition,
            responsiveRefreshRate: 200,
        });

        testi2.owlCarousel({
            items: 4,
            itemsDesktop: [1199, 4],
            itemsDesktopSmall: [979, 4],
            itemsTablet: [768, 4],
            itemsMobile: [479, 3],
            pagination: false,
            navigation: false,
            responsiveRefreshRate: 100,
            afterInit: function (el) {
                el.find(".owl-item").eq(0).addClass("synced");
            }
        });

        $( testi2 ).on("click", ".owl-item", function (e) {
            e.preventDefault();
            var number = $(this).data("owlItem");
            testi1.trigger("owl.goTo", number);
        });

        function syncPosition(el) {
            var current = this.currentItem;
            $( testi2 )
                .find(".owl-item")
                .removeClass("synced")
                .eq(current)
                .addClass("synced")
            if ($(".slider-images").data("owlCarousel") !== undefined) {
                center(current)
            };
        }

       

        function center(number) {
            var testi2visible = testi2.data("owlCarousel").owl.visibleItems;
            var num = number;
            var found = false;
            for (var i in testi2visible) {
                if (num === testi2visible[i]) {
                    var found = true;
                }
            }

            if (found === false) {
                if (num > testi2visible[testi2visible.length - 1]) {
                    testi2.trigger("owl.goTo", num - testi2visible.length + 2)
                } else {
                    if (num - 1 === -1) {
                        num = 0;
                    }
                    testi2.trigger("owl.goTo", num);
                }
            } else if (num === testi2visible[testi2visible.length - 1]) {
                testi2.trigger("owl.goTo", testi2visible[1])
            } else if (num === testi2visible[0]) {
                testi2.trigger("owl.goTo", num - 1)
            }
        }
    });
    
 
    
})(jQuery);


